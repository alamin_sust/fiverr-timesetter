<%@page import="timesetter.Database"%>
<%@page import="timesetter.TimeSetter"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="static timesetter.TimeSetter.checkNullOrEmpty" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.TimeZone" %>


<%
    Database db = new Database();
    db.connect();
    try {
    Statement st = db.connection.createStatement();
    ResultSet rs;
    Statement st2 = db.connection.createStatement();
    ResultSet rs2;



    String lati =  request.getParameter("lat");
    String longi =  request.getParameter("long");
    String deviceId =  request.getParameter("deviceId");
    String time =  request.getParameter("time");
    String deviceTime =  request.getParameter("deviceTime");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        Date dateTime = simpleDateFormat.parse(time);
        Date deviceDateTime = simpleDateFormat.parse(deviceTime);

        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date serverCurrentDateTime = simpleDateFormat.parse(simpleDateFormat2.format(new Date()));

        Calendar calendarDateTime = Calendar.getInstance();
        calendarDateTime.setTime(dateTime);

        Calendar calendarDeviceDateTime = Calendar.getInstance();
        calendarDeviceDateTime.setTime(deviceDateTime);

        Calendar calendarServerCurrentDateTime = Calendar.getInstance();
        calendarServerCurrentDateTime.setTime(serverCurrentDateTime);

        long miliSecsToAdd = calendarServerCurrentDateTime.getTimeInMillis()-calendarDeviceDateTime.getTimeInMillis();

        long expiryTimeInMiliseconds = calendarDateTime.getTimeInMillis() + miliSecsToAdd;

    if(checkNullOrEmpty(lati) ||checkNullOrEmpty(longi) ||checkNullOrEmpty(time)
            ||checkNullOrEmpty(deviceId) ||checkNullOrEmpty(deviceTime)) {
        throw new Exception();
    }

    String q = "insert into times (latitude,longitude,device_id,time,device_time,miliseconds_to_add,expiry_time_in_miliseconds) values ('"+lati+"','"+longi+"','"+deviceId+"','"+time+"','"+deviceTime+"','"+miliSecsToAdd+"','"+expiryTimeInMiliseconds+"')";

    st.executeUpdate(q);

    String q2 = "SELECT LAST_INSERT_ID() id";
    rs2=st2.executeQuery(q2);
    rs2.next();


%>
{
"Result" : true,
"Id" : <%=rs2.getString("id")%>
}
<%} catch (Exception e){  %>
{
"Result" : false
}
<%} finally {
    db.close();
}%>
