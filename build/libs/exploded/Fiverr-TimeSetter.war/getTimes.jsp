<%@page import="timesetter.Database"%>
<%@page import="timesetter.TimeSetter"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@ page import="static timesetter.TimeSetter.checkNullOrEmpty" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.TimeZone" %>


<%
    Database db = new Database();
    db.connect();
    try {
        Statement st = db.connection.createStatement();
        ResultSet rs;
        Statement st2 = db.connection.createStatement();
        ResultSet rs2;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date serverCurrentDateTime = simpleDateFormat.parse(simpleDateFormat2.format(new Date()));

        Calendar calendarServerCurrentDateTime = Calendar.getInstance();
        calendarServerCurrentDateTime.setTime(serverCurrentDateTime);
        long serverCurrentTimeInMiliSeconds = calendarServerCurrentDateTime.getTimeInMillis();

        String q = "select DISTINCT longitude,latitude from times w" +
                "here expiry_time_in_miliseconds>='"+serverCurrentTimeInMiliSeconds+"'";
        rs = st.executeQuery(q);

        ArrayList<String> latList = new ArrayList<>();
        ArrayList<String> longList = new ArrayList<>();
        ArrayList<String> timeList = new ArrayList<>();

        String globalStr = "";
        int gIter = 0;

        while (rs.next()) {
            globalStr+=(gIter>0?",{":"{");
            String lati = rs.getString("latitude");
            String longi = rs.getString("longitude");

            globalStr+="\""+lati+"\",";
            globalStr+="\""+longi+"\",";

            latList.add(lati);
            longList.add(longi);
            String q2="select * from times where longitude='"+longi+"' and latitude='"+lati+"' and expiry_time_in_miliseconds>='"+serverCurrentTimeInMiliSeconds+"'";
            rs2= st2.executeQuery(q2);
            int iter = 0;
            String temp="";
            while (rs2.next()) {
                temp+=(iter>0?",{":"{");

                temp+="\"id\":\""+rs2.getString("id")+"\",";
                temp+="\"deviceId\":\""+rs2.getString("device_id")+"\",";
                temp+="\"time\":\""+rs2.getString("time")+"\"";
                temp+="}";
                iter++;
            }
            timeList.add(temp);
            globalStr+="\"Time\" : [";
            globalStr+=temp;
            globalStr+="]";
            globalStr+="}";
            gIter++;
        }

        /*{
            "Result" : true,
            "Times" :
            [
                {
                    "93.31",
                    "29.43",
                    "Time" : [
                        {
                            "2",
                            "1234abcd",
                            "20/03/2018 23:08"
                        },
                        {
                            "4",
                            "7543lkas",
                            "20/03/2018 23:08"
                        }
                    ]
                },
                {
                    "97.45",
                    "30.11",
                    "Time" :
                    [
                        {
                            "3",
                            "3213bfgh",
                            "19/03/2018 03:08"
                        }
                    ]
                }
            ]
        }*/

%>
{
"Result" : true,
"Times" : [ <%=globalStr%> ]
}
<%} catch (Exception e){  %>
{
"Result" : false
}
<%} finally {
    db.close();
}%>

